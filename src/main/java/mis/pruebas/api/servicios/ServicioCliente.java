package mis.pruebas.api.servicios;

import mis.pruebas.api.modelos.Cliente;
import mis.pruebas.api.modelos.Cuenta;

import java.util.List;

public interface ServicioCliente {

    //CRUD

    public List<Cliente> obtenerClientes(int pagina, int cantidad);
    //CREATE
    public void insertarClienteNuevo(Cliente cliente);

    //READ
    public Cliente obtenerCliente(String documento);

    //update (solamente modificar n ocrear)
    public void guardarCliente(Cliente cliente);
    public void emparcharCliente(Cliente parche);


    //delete
    public void borrarCliente(String documento);

    public void agregarCuentaCliente(String documento, String numeroCuenta);

    public List<String> obtenerCuentasCliente(String documento);
}
