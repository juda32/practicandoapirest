package mis.pruebas.api.servicios.impl;


import mis.pruebas.api.modelos.Cliente;
import mis.pruebas.api.modelos.Cuenta;
import mis.pruebas.api.servicios.ServicioCuenta;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class ServicioCuentaImpl implements ServicioCuenta {

    public final Map<String, Cuenta> cuenta = new ConcurrentHashMap<>();

    @Override
    public List<Cuenta> obtenerCuentas(int pagina, int cantidad) {
        List<Cuenta> cuentaContados = List.copyOf(this.cuenta.values());
        int indiceInicial = pagina * cantidad;
        int indiceFinal = indiceInicial + cantidad;
        if (indiceFinal > cuentaContados.size()) {
            indiceFinal = cuentaContados.size();
        }
        return cuentaContados.subList(indiceInicial, indiceFinal);
    }

    @Override
    public void insertarCuentaNueva(Cuenta cuenta) {
        this.cuenta.put(cuenta.numero, cuenta);

    }

    @Override
    public Cuenta obtenerCuenta(String numero) {
        if (!this.cuenta.containsKey(numero))
            throw new RuntimeException("No existe la cuenta: " + numero);
        return this.cuenta.get(numero);
    }

    @Override
    public void guardarCuenta(Cuenta cuenta) {
        if (!this.cuenta.containsKey(cuenta.numero))
            throw new RuntimeException("No existe la cuenta: " + cuenta.numero);
        // piso el objeto en el hashmap
        this.cuenta.replace(cuenta.numero, cuenta);


    }

    @Override
    public void emparcharCuenta(Cuenta parche) {
        final Cuenta existente = this.cuenta.get(parche.numero);
        if (parche.moneda != existente.moneda)
            existente.moneda = parche.moneda;
        if (parche.saldo != null)
            existente.saldo = parche.saldo;
        if (parche.tipo != null)
            existente.tipo = parche.tipo;
        if (parche.estado != null)
            existente.estado = parche.estado;
        if (parche.oficina != null)
            existente.oficina = parche.oficina;
        this.cuenta.replace(existente.numero, existente);

    }

    @Override
    public void borrarCuenta(String numero) {
        if (!this.cuenta.containsKey(numero))
            throw new RuntimeException("No existe la cuenta: " + numero);
        this.cuenta.remove(numero);
    }
}
