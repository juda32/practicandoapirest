package mis.pruebas.api.controlador;

public class Rutas {
    public static final String BASE ="/api/v1";
    public static final String CLIENTES = BASE +"/clientes";
    public static final String CUENTAS = BASE + "/cuentas";
    public static final String CUENTAS_CLIENTE = "api/v1/clientes/{documento}/cuentas";
}
