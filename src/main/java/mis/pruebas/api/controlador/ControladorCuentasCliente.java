package mis.pruebas.api.controlador;


import mis.pruebas.api.modelos.Cliente;
import mis.pruebas.api.modelos.Cuenta;
import mis.pruebas.api.servicios.ServicioCliente;
import mis.pruebas.api.servicios.ServicioCuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.websocket.ClientEndpoint;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping(Rutas.CLIENTES + "/{documento}/cuentas")
public class ControladorCuentasCliente {

    //CRUD .GET *,GET,POST,PUT,PATCH

    @Autowired
    ServicioCliente servicioCliente;
    @Autowired
    ServicioCuenta servicioCuenta;

    public static class DatosEntradaCuenta {
        public String codigoCuenta;
    }
    // Post http://localhost:9060/api/v1/clientes/12345678/cuentas + Datos --> agregarCuentaClientes
    @PostMapping
    public ResponseEntity agregarCuentaCliente(@PathVariable String documento,
                                                       @RequestBody DatosEntradaCuenta datosEntradaCuenta){
        try {
            this.servicioCliente.agregarCuentaCliente(documento, datosEntradaCuenta.codigoCuenta);
        }catch (Exception X){
            return ResponseEntity.notFound().build();
        }
        return  ResponseEntity.ok().build();
    }
    // GET http://localhost:9060/api/v1/clientes/12345678/cuentas + Datos --> agregarCuentaClientes
    @GetMapping
    public ResponseEntity<List<String>> obtenerCuentasCliente(@PathVariable String documento){
        try{
            final var cuentasCliente =  this.servicioCliente.obtenerCuentasCliente(documento);
            return ResponseEntity.ok(cuentasCliente);
        } catch (Exception x){
            return ResponseEntity.notFound().build();
        }
    }
    @GetMapping("/{numeroCuenta}")
    public ResponseEntity<Cuenta> obtenerCuentaCliente(@PathVariable String documento,
                                                       @PathVariable String numeroCuenta){
        try {
            final Cliente cliente = this.servicioCliente.obtenerCliente(documento);
            if (!cliente.codigosCuentas.contains(numeroCuenta))
                throw new ResponseStatusException(HttpStatus.NOT_FOUND);

            final Cuenta cuenta = this.servicioCuenta.obtenerCuenta(numeroCuenta);
        }catch (Exception x){
             throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{numeroCuenta}")
    public ResponseEntity<Cuenta> eliminarCuentaCliente(@PathVariable String documento,
                                                       @PathVariable String numeroCuenta) {
        try {
            final Cliente cliente = this.servicioCliente.obtenerCliente(documento);
            final Cuenta cuenta = this.servicioCuenta.obtenerCuenta(numeroCuenta);
            if (cliente.codigosCuentas.contains(numeroCuenta)) {
                cuenta.estado = "INACTIVA";
                cliente.codigosCuentas.remove(numeroCuenta);
            }
        } catch (Exception x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.noContent().build();
    }
}
