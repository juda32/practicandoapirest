package mis.pruebas.api.controlador;


import mis.pruebas.api.modelos.Cuenta;
import mis.pruebas.api.servicios.ServicioCuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping(Rutas.CUENTAS)
public class ControladorCuenta {

    @Autowired
    ServicioCuenta servicioCuenta;

    //GET http://localhost:9000/api/v1/clientes -> List<Cliente> obtenerClientes()
    @GetMapping
    public List<Cuenta> obtenerCuentas(@RequestParam int pagina, @RequestParam int cantidad) {
        try {
            return this.servicioCuenta.obtenerCuentas(pagina - 1, cantidad);
        } catch (Exception x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }


    }

    @PostMapping
    public void agregarCuenta(@RequestBody Cuenta cuenta) {
        this.servicioCuenta.insertarCuentaNueva(cuenta);
    }

    @GetMapping("/{numero}")
    public Cuenta obtenerUnaCuenta(String numero) {
        try {
            return this.servicioCuenta.obtenerCuenta(numero);
        } catch (Exception x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{numero}")
    public void reemplazarUnaCuenta(@PathVariable("numero") String nroCuenta,
                                    @RequestBody Cuenta cuenta ){
        try {
            cuenta.numero = nroCuenta;
            this.servicioCuenta.guardarCuenta(cuenta);
        }catch (Exception x){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PatchMapping("/{numero}")
    public void emparcharUnaCuenta(@PathVariable("numero") String nroCuenta,
                                   @RequestBody Cuenta cuenta){
        cuenta.numero = nroCuenta;
        this.servicioCuenta.emparcharCuenta(cuenta);
    }

    @DeleteMapping("/{numero}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void eliminarUnaCuenta(@PathVariable String numero){
        try {
            this.servicioCuenta.borrarCuenta(numero);
        } catch (Exception x) {}
    }
}