package mis.pruebas.api.modelos;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;

public class Cliente {
    public String documento;
    public String nombre;
    public String edad;
    public String fechaNacimiento;
    public String telefono;
    public String correo;
    public String direccion;

    @JsonIgnore
    public List<String> codigosCuentas = new ArrayList<>();

}
